# GALEX

```ascii
 @@@@@@@   @@@@@@  @@@      @@@@@@@@ @@@  @@@
!@@       @@!  @@@ @@!      @@!      @@!  !@@
!@! @!@!@ @!@!@!@! @!!      @!!!:!    !@@!@!
:!!   !!: !!:  !!! !!:      !!:       !: :!!
 :: :: :   :   : : : ::.: : : :: ::  :::  :::
```

GALEX is an open source Go lang based emulation software