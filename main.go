//  @@@@@@@   @@@@@@  @@@      @@@@@@@@ @@@  @@@
// !@@       @@!  @@@ @@!      @@!      @@!  !@@
// !@! @!@!@ @!@!@!@! @!!      @!!!:!    !@@!@!
// :!!   !!: !!:  !!! !!:      !!:       !: :!!
//  :: :: :   :   : : : ::.: : : :: ::  :::  :::

package main

import "gitlab.com/azuwey/galex/gfx"

func main() {
	scale := 3
	gfx.InitWindow("GALEX", 480*scale, 320*scale)
	gfx.ShowsWindow()
}
