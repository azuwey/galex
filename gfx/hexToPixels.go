package gfx

import (
	"fmt"
	"strconv"
)

// hexToPixels calculate RGBA pixels from a 8 bit hex value
func hexToPixels(icon [32 * 32]uint32) []uint8 {
	pixels := []uint8{}

	for _, hex := range icon {
		if R, err := strconv.ParseUint(fmt.Sprintf("%X", hex)[0:2], 16, 32); err != nil {
			pixels = append(pixels, 0)
		} else {
			convertR := (uint8)(R)
			pixels = append(pixels, convertR)
		}
		if G, err := strconv.ParseUint(fmt.Sprintf("%X", hex)[2:4], 16, 32); err != nil {
			pixels = append(pixels, 0)
		} else {
			convertG := (uint8)(G)
			pixels = append(pixels, convertG)
		}
		if B, err := strconv.ParseUint(fmt.Sprintf("%X", hex)[4:6], 16, 32); err != nil {
			pixels = append(pixels, 0)
		} else {
			convertB := (uint8)(B)
			pixels = append(pixels, convertB)
		}
		if A, err := strconv.ParseUint(fmt.Sprintf("%X", hex)[6:8], 16, 32); err != nil {
			pixels = append(pixels, 0)
		} else {
			convertA := (uint8)(A)
			pixels = append(pixels, convertA)
		}
	}

	return pixels
}
