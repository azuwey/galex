package gfx

import (
	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
)

// Handle the window drawing
func draw(window *glfw.Window, program uint32) {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	gl.UseProgram(program)

	glfw.PollEvents()
	window.SwapBuffers()
}

// Handle the window event loop
/*func eventLoop(w *glfw.Window) {
	// Calculate layout sizes
	var menubarSize = image.Point{
		X: w.Bounds.Max().X - 8,                            // Width
		Y: int(math.Floor(float64(w.Bounds.Max().Y) / 10)), // Height
	}
	var canvasSize = image.Point{
		X: w.Bounds.Max().X - 8,                  // Width
		Y: w.Bounds.Max().Y - menubarSize.Y - 11, // Height
	}

	// Draw menubar
	w.Row(menubarSize.Y).Static(menubarSize.X)
	if gw := w.GroupBegin("menubar_group", defaultGroupFlag); gw != nil {
		button(gw, "GAMES", assets.GamesIcon)
		gw.GroupEnd()
	}

	// Draw canvas
	w.Row(canvasSize.Y).Static(canvasSize.X)
	if gw := w.GroupBegin("canvas_group", defaultGroupFlag); gw != nil {
		gw.GroupEnd()
	}
}*/
