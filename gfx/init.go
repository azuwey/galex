package gfx

import (
	"log"
	"runtime"

	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
)

var window *glfw.Window
var program uint32

// InitWindow is initialize a new window
func InitWindow(title string, width, height int) {
	runtime.LockOSThread()

	window = initGlfw(title, width, height)
	program = initOpenGL()
}

// initGlfw initializes glfw and returns a Window to use.
func initGlfw(title string, width, height int) *glfw.Window {
	if err := glfw.Init(); err != nil {
		panic(err)
	}

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 3)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	win, err := glfw.CreateWindow(width, height, title, nil, nil)
	if err != nil {
		panic(err)
	}
	win.MakeContextCurrent()

	return win
}

// initOpenGL initializes OpenGL and returns an intiialized program.
func initOpenGL() uint32 {
	if err := gl.Init(); err != nil {
		panic(err)
	}
	version := gl.GoStr(gl.GetString(gl.VERSION))
	log.Println("OpenGL version", version)

	prog := gl.CreateProgram()
	gl.LinkProgram(prog)

	return prog
}

// ShowsWindow is shows window, runs event loop
func ShowsWindow() {
	defer glfw.Terminate()
	for !window.ShouldClose() {
		draw(window, program)
	}
}
